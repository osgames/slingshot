#    This file is part of Slingshot.
#
# Slingshot is a two-dimensional strategy game where two players attempt to shoot one
# another through a section of space populated by planets.  The main feature of the
# game is that the shots, once fired, are affected by the gravity of the planets.

# Slingshot is Copyright 2007 Jonathan Musther and Bart Mak. It is released under the
# terms of the GNU General Public License version 2, or later if applicable.

# Slingshot is free software; you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation; either
# version 2 of the License, or any later version.

# Slingshot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Slingshot;
# if not, write to
# the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA 


import pygame
from settings import *
from general import *
class MenuItem:
        def __init__(self, name, dim = True):
                self.name = name
                self.color = (0, 0, 255)
                self.selected = False # unselected by default
                self.enabled = True # enabled by default
                self.color_anim = 15 # either increment or decrement the color value while the item is selected.
                self.dim = dim

        def draw(self): return
        def draw(self, surface, x, y):
                # if the item is currently selected, we have to play a little bit with its color (to make some animation)
                if self.selected == True:
                        if (self.color[0] + self.color_anim) >= 255 or (self.color[0] + self.color_anim) <= 0:
                                self.color_anim *= -1
                        self.color = ( self.color[0] + self.color_anim, self.color[1] + self.color_anim, 255 )
                                
                # in any other cases, the color should be the good one to apply.
                text = Settings.menu_font.render(self.name, 1, self.color)
                surface.blit(text, (x, y))

        def toggle_select(self):
                self.selected = not self.selected
                self.color = (0, 0, 255)

        def toggle_enable(self):
                self.set_enable(not self.enabled)

        def set_enable(self, enabled):
                self.enabled = enabled
                if not self.enabled:
                        self.color = (75, 75, 75)
                else:
                        self.color = (0, 0, 255)

        
                

class MenuTick(MenuItem):
        def __init__(self, name, dim = True):
                MenuItem.__init__(self, name, dim)
                self.ticked = False


        def draw(self, surface, x, y):
                # y - 8 for box alignment.
                surface.blit(Settings.box, (x, y - 8))
                if self.ticked:
                        if self.enabled:
                                surface.blit(Settings.tick, (x, y - 8))
                        else:
                                surface.blit(Settings.tick_inactive, (x, y - 8))

                # Now, draw the text at the correct position, relative to the tickbox.                    
                MenuItem.draw(self, surface, x + 35, y)

        def toggle_tick(self):
                self.ticked = not self.ticked
                
class Menu(MenuItem):
        def __init__(self, name, dim = True):
                MenuItem.__init__(self, name, dim)
                self.reset()
                self.items = []
                self.selected = -1

        def reset(self):
                self.selected = 0
                self.choice = ""

        def draw(self):
                w = self.get_width()
                h = self.get_height()
                result = pygame.Surface((w, h))
                #result.fill((100,0,0))
                result.blit(Settings.menu_background, (0,0))
                
                txt = Settings.menu_font.render(self.name, 1, (255,255,255))
                rect = txt.get_rect()
                rect.midtop = (w / 2, Settings.MENU_LINEFEED)
                result.blit(txt, rect.topleft)

                y = 2.5 * Settings.MENU_LINEFEED

                for item in self.items:
                        item.draw( result, 25, y )
                        y += Settings.MENU_LINEFEED
                                

                ############################################################
                pygame.draw.rect(result, (0,0,200), pygame.Rect(0, 0, w, h), 1)               
                return result

        def add(self, item):
                self.items.append(MenuItem(item))
                if self.selected < 0:
                        self.selected = 0
                        self.items[ self.selected ].toggle_select()
                        
        def addoption(self, name, ticked = False, enabled = True):
                new_tick = MenuTick(name)
                new_tick.set_enable(enabled)
                new_tick.ticked = ticked
                self.items.append(new_tick)

        def change_active(self, name, enabled):
                for item in self.items:
                        if item.name == name:
                                item.set_enable(enabled)
                                break

        def move(self, direction):
                # Unselect the current item
                self.items[ self.selected ].toggle_select()
                # Move around
                self.selected += direction
                # clamp
                if self.selected < 0:
                        self.selected = len( self.items ) - 1
                elif self.selected >= len( self.items ):
                        self.selected = 0
                # Select the new item
                self.items[ self.selected ].toggle_select()
                
        def up(self):
                self.move(-1) # Move UP

        def left(self):
                self.up()

        def down(self):
                self.move(1) # move DOWN

        def right(self):
                self.down()

        def select(self):
                self.choice = self.items[ self.selected ].name
                try:
                        self.items[ self.selected ].toggle_tick()
                except:
                        pass
                return self.choice

        def get_choice(self):
               c = self.choice
               self.choice = ""
               return c

        def get_width(self):
                #return 300
                return 350
        
        def get_height(self):
                #n = self.items.__len__()
                #return 44 * n + 100
                return 500
                                
class Help(MenuItem):
        
        def __init__(self):
                MenuItem.__init__(self, "", False)
                self.img, rect = load_image("data/help.png", (0,0,0))
                self.choice = ""
                
        def select(self):
                self.choice = "Back"
        
        def draw(self):
                return self.img

        def get_choice(self):
                c = self.choice
                self.choice = ""
                return c
        
class Welcome(MenuItem):
        
        def __init__(self):
                MenuItem.__init__(self, "")
                self.img, rect = load_image("data/welcome.png", (0,0,0))
                self.choice = ""
                
        def select(self):
                self.choice = "Start"
        
        def draw(self):
                return self.img
        
        def get_choice(self):
                c = self.choice
                self.choice = ""
                return c

class Numeric(MenuItem):
        
        def __init__(self, txt, init, step, mmax, mmin = 0, inf = "0"):
                MenuItem.__init__(self, txt)
                self.val = init
                self.step = step
                self.mmax = mmax
                self.mmin = mmin
                self.inf = inf
                self.choice = -1
                
        def up(self):
                self.val += self.step
                if self.val > self.mmax:
                        self.val = self.mmax
        
        def down(self):
                self.val -= self.step
                if self.val < self.mmin:
                        self.val = self.mmin
                        
        def select(self):
                self.choice = self.val

        def get_choice(self):
                c = self.choice
                self.choice = -1
                return c
                
        def draw(self):
                w = self.get_width()
                h = self.get_height()
                result = pygame.Surface((w, h))
                
                result.blit(Settings.menu_background, (0,0))
                
                txt = Settings.menu_font.render(self.name, 1, (255,255,255))
                rect = txt.get_rect()
                rect.midtop = (w / 2, Settings.MENU_LINEFEED)
                result.blit(txt, rect.topleft)
                
                if self.val == 0:
                        txt = Settings.menu_font.render(self.inf, 1, (255,255,255))
                else:
                        txt = Settings.menu_font.render("%d" %(self.val), 1, (255,255,255))
                rect = txt.get_rect()
                rect.midtop = (w / 2, 2.5 * Settings.MENU_LINEFEED)
                result.blit(txt, rect.topleft)
                
                pygame.draw.rect(result, (0,0,200), pygame.Rect(0, 0, w, h), 1)
                
                return result
                
        
class Confirm(Menu):
        
        def __init__(self, txt1, txt2 = "", txt3 = ""):
                Menu.__init__(self, "")
                self.txt1 = txt1
                self.txt2 = txt2
                self.txt3 = txt3
        
        def draw(self):
                offset = 0
                
                w = self.get_width()
                h = self.get_height()
                result = pygame.Surface((w, h))
                #result.fill((100,0,0))
                result.blit(Settings.menu_background, (0,0))
                
                txt = Settings.menu_font.render(self.txt1, 1, (255,255,255))
                rect = txt.get_rect()
                rect.midtop = (w / 2, Settings.MENU_LINEFEED)
                result.blit(txt, rect.topleft)
                
                if self.txt2 != "":
                        offset += Settings.MENU_LINEFEED
                        txt = Settings.menu_font.render(self.txt2, 1, (255,255,255))
                        rect = txt.get_rect()
                        rect.midtop = (w / 2, Settings.MENU_LINEFEED + offset)
                        result.blit(txt, rect.topleft)
                
                if self.txt3 != "":
                        offset += Settings.MENU_LINEFEED
                        txt = Settings.menu_font.render(self.txt3, 1, (255,255,255))
                        rect = txt.get_rect()
                        rect.midtop = (w / 2, Settings.MENU_LINEFEED + offset)
                        result.blit(txt, rect.topleft)
                
                for i in range(0, 2):
                       # if i == self.selected:
                       #         color = (self.count,self.count,255)
                       # else:
                       #         color = (0,0,255)
##                        txt = Settings.menu_font.render(self.items[i][0], 1, color)
##                        rect = txt.get_rect()
                        if i == 0:
                                self.items[i].draw( result, w / 2 - Settings.MENU_LINEFEED, 3 * Settings.MENU_LINEFEED + offset)
                        else:
                                self.items[i].draw( result, w / 2 + Settings.MENU_LINEFEED, 3 * Settings.MENU_LINEFEED + offset)
##                        result.blit(txt, rect.topleft)
                
                pygame.draw.rect(result, (0,0,200), pygame.Rect(0, 0, w, h), 1)
                
##                self.count += self.inc
##                if self.count > 255 or self.count < 0:
##                        self.inc *= -1
##                        self.count += 2* self.inc
                

                return result
