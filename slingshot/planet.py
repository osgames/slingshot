#    This file is part of Slingshot.
#
# Slingshot is a two-dimensional strategy game where two players attempt to shoot one
# another through a section of space populated by planets.  The main feature of the
# game is that the shots, once fired, are affected by the gravity of the planets.

# Slingshot is Copyright 2007 Jonathan Musther and Bart Mak. It is released under the
# terms of the GNU General Public License version 2, or later if applicable.

# Slingshot is free software; you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation; either
# version 2 of the License, or any later version.

# Slingshot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Slingshot;
# if not, write to
# the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA 


import pygame
import math
from random import randint

from math import sqrt
from settings import *
from general import *

class Planet(pygame.sprite.Sprite):
        def __init__(self, nid, image_name):
                pygame.sprite.Sprite.__init__(self)
                self.image, self.rect = load_image(image_name, (0,0,0))
                self.id = nid

                #m = randint(0, (512-8)**2)
		#self.mass = 8 + sqrt(m)
		self.mass = randint(8,512)
		self.r = self.mass**(1.0/3.0) * 12.5
		
		s = round(2 * self.r / 0.96)
		self.image = pygame.transform.scale(self.image, (s, s))
		self.pos = (0, 0)

        def set_pos(self, pos):
                self.pos = pos
                
        def get_n(self):
                return self.id
	
	def get_radius(self):
		return self.r
		
	def get_mass(self):
		return self.mass
	
	def get_pos(self):
		return self.pos

	def get_image(self):
                return self.image
	
	def fade(self, f):
		self.image = self.fade_image
		self.image.set_alpha(255 - round(f * 2.55))
#	def fade(self):
#		self.image = self.fade_image
#		self.image.blit(self.orig, (0,0))
#		for i in range(1, f):
#			r = round(self.r / 10)
#			pygame.draw.circle(self.image, (0,0,0,0), (randint(r, round(2 * self.r) - r), randint(r, round(2 * self.r) - r)), r)
#			for j in range(0, self.r):
#				self.image.set_at((randint(0, round(self.r * 2)), randint(0, round(self.r * 2))), (0,0,0,0))
                

class Planets(pygame.sprite.Group):
	
	def __init__(self, planets):
                pygame.sprite.Group.__init__(self)
		planet_list = []

		while planets > 0:
                        n = randint(1, 8)
                        if n not in planet_list:
                                filename = "data/planet_%d.png" %(n)
                                planet = Planet(n, filename)		
                                positioned = False
                                while not positioned:
                                        pos = (randint(Settings.PLANET_SHIP_DISTANCE + round(planet.get_radius()), \
                                                800 - Settings.PLANET_SHIP_DISTANCE - round(planet.get_radius())), \
                                                randint(Settings.PLANET_EDGE_DISTANCE + round(planet.get_radius()), \
                                                600 - Settings.PLANET_EDGE_DISTANCE - round(planet.get_radius())) \
                                              )
                                        positioned = True
                                        for p in self.sprites():
                                                d = math.sqrt((planet.get_pos()[0] - p.get_pos()[0])**2 + (planet.get_pos()[1] - p.get_pos()[1])**2)
                				if d < (planet.get_radius() + p.get_radius()) * 1.5 + 0.1 * (planet.get_mass() + p.get_mass()):
                					positioned = False
		
#                               self.rect = self.image.get_rect()
#                		self.rect.center = self.pos
#                               rect = background.blit(self.image, self.rect.topleft)
#                               self.fade_image = pygame.Surface(self.image.get_size())
#                               self.fade_image.blit(background, (0,0), rect)
#                               self.fade_image.set_alpha(255)
#                               self.fade_image.convert()
                                planets = planets - 1
                                self.add(planet)
                                planet_list.append(n)

		

