#    This file is part of Slingshot.
#
# Slingshot is a two-dimensional strategy game where two players attempt to shoot one
# another through a section of space populated by planets.  The main feature of the
# game is that the shots, once fired, are affected by the gravity of the planets.

# Slingshot is Copyright 2007 Jonathan Musther and Bart Mak. It is released under the
# terms of the GNU General Public License version 2, or later if applicable.

# Slingshot is free software; you can redistribute it and/or modify it under the terms
# of the GNU General Public License as published by the Free Software Foundation; either
# version 2 of the License, or any later version.

# Slingshot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.

# You should have received a copy of the GNU General Public License along with Slingshot;
# if not, write to
# the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA 


class Settings:
        g = 120 # gravity
        MAXPOWER = 350
        PLANET_SHIP_DISTANCE = 75 # this is actually the distance towards the edge left and right
        PLANET_EDGE_DISTANCE = 50 # upper and lower edge
        
        PARTICLE_5_MINSPEED = 100
        PARTICLE_5_MAXSPEED = 200 # 200: easy, 300: wild
        PARTICLE_10_MINSPEED = 150
        PARTICLE_10_MAXSPEED = 250 # 250 easy, 400-500 wild
        n_PARTICLES_5 = 20  # number of small particles originating from a big one
        n_PARTICLES_10 = 30 # number of big particles originating from explosion
                           # if both are too high, the game stalls on impact
        
        ROTATE = True
        BOUNCE = False
        FIXED_POWER = False
        PARTICLES = True
        INVISIBLE = False
        RANDOM = False
        POWER = 200
        
        TIMEOUT = 750
        #FIXME: remove this one
        MAX_FLIGHT = TIMEOUT
        
        MAX_PLANETS = 4
        
        HITSCORE = 1500
        SELFHIT = 2000
        QUICKSCORE1 = 500
        QUICKSCORE2 = 200
        QUICKSCORE3 = 100
        
        PENALTY_FACTOR = 5
        
        FPS = 30
        KEY_REPEAT = 30 # time between repeating key events, keep a little lower than 1000 / FPS
        KEY_DELAY = 250
        
        MENU_FONT_SIZE = 26
        MENU_LINEFEED = 36
        
        MAX_ROUNDS = 0
        def load(self, path):
                f=open(path, 'r')
                lines = f.readlines()
                for l in lines:
                        tokens = l.split()
                        if tokens[0] == "Bounce:":
                                if tokens[1] == "1":
                                        self.BOUNCE = True
                        if tokens[0] == "Fixed_Power:":
                                if tokens[1] == "1":
                                        self.FIXED_POWER = True
                        elif tokens[0] == "Particles:":
                                if tokens[1] == "1":
                                        self.PARTICLES = True
                        elif tokens[0] == "Random:":
                                if tokens[1] == "1":
                                        self.RANDOM = True
                        elif tokens[0] == "Invisible:":
                                if tokens[1] == "1":
                                        self.INVISIBLE = True
                        elif tokens[0] == "Max_Planets:":
                                self.MAX_PLANETS = int(tokens[1])
                        elif tokens[0] == "Timeout:":
                                self.TIMEOUT = int(tokens[1])
                        elif tokens[0] == "Rounds:":
                                self.MAX_ROUNDS = int(tokens[1])
                f.close()

                        
        def save(self, path):
                f = file(path, 'wt')
                if self.BOUNCE:
                        f.write("Bounce: 1\n")
                else:
                        f.write("Bounce: 0\n")
                if self.FIXED_POWER:
                        f.write("Fixed_Power: 1\n")
                else:
                        f.write("Fixed_Power: 0\n")
                if self.INVISIBLE:
                        f.write("Invisible: 1\n")
                else:
                        f.write("Invisible: 0\n")
                if self.RANDOM:
                        f.write("Random: 1\n")
                else:
                        f.write("Random: 0\n")
                if self.PARTICLES:
                        f.write("Particles: 1\n")
                else:
                        f.write("Particles: 0\n")
                f.write("Max_Planets: %d\n" %(self.MAX_PLANETS))
                f.write("Timeout: %d\n" %(self.TIMEOUT))
                f.write("Rounds: %d\n" %(self.MAX_ROUNDS))
                f.close()
